// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'reset-css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUser, faComment, faSuitcase, faEnvelope, faSignal, faCog, faLifeRing, faCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueSlideUpDown from 'vue-slide-up-down'
import 'bootstrap/dist/css/bootstrap.min.css'
import VueChartJs from 'vue-chartjs'

library.add(faUser, faComment, faSuitcase, faEnvelope, faSignal, faCog, faLifeRing, faCircle)
Vue.use(VueChartJs)
Vue.component('line-chart', {
  extends: VueChartJs.Line,
  mounted () {
    this.renderChart({
      labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      datasets: [
        {
          label: 'Data One',
          backgroundColor: 'transparent',
          borderColor: '#F6AF39',
          data: [20, 40, 10, 40, 39, 20, 40]
        },
        {
          label: 'Data Two',
          backgroundColor: 'transparent',
          borderColor: '#EB3D6C',
          data: [0, 10, 20, 5, 15, 30, 35]
        },
        {
          label: 'Data Three',
          backgroundColor: 'transparent',
          borderColor: '#55BF3A',
          data: [20, 10, 0, 10, 20, 40, 35]
        }
      ]
    }, {responsive: true, maintainAspectRatio: false})
  }
})
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('vue-slide-up-down', VueSlideUpDown)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
